function clickealo(){
	url ="pages/admin/index.php";
	$.post(url, function (responseText){
		$(".col-md-9").html(responseText);
	});
}

function eliminar_unidad(id){
	$.ajax({
		url: 'core/data.action.delete.php',
		type: 'POST',
		data: {idu:id, table:'unidad'},
		success: function(data){
			console.log(data);
			clickealo();
		}
	});
}

function editar_unidad(id){
	$("#myModal2").modal("show");

	url ="pages/admin/edit.php";
	$.post(url,{idu:id}, function (responseText){
		$(".modal-body_editar").html(responseText);
	});
}

function actualizarunidad(){
	console.log('SI');
	var u_Id = $('#up_id').val();
	var u_Email = $('#up_Email').val();
	var u_Nombre= $('#up_Nombre').val();
	var u_Telefono= $('#up_Telefono').val();
	var u_Localidad= $('#up_Localidad').val();
	var u_Municipio= $('#up_Municipio').val();
	var u_Estado= $('#up_Estado').val();
	var u_Actividad= $('#up_Actividad').val();

	$.ajax({
		url: 'controller/unidad.update.php',
		type: 'POST',
		data: {Email:u_Email,Nombre:u_Nombre,Telefono:u_Telefono,
		Localidad:u_Localidad,Municipio:u_Municipio,Estado:u_Estado,
		Actividad: u_Actividad, Id:u_Id},
		success: function(data){
			console.log(data);
		}
	});

}

function guardarunidad(){
	var u_Email = $('#u_Email').val();
	var u_Nombre= $('#u_Nombre').val();
	var u_Telefono= $('#u_Telefono').val();
	var u_Localidad= $('#u_Localidad').val();
	var u_Municipio= $('#u_Municipio').val();
	var u_Estado= $('#u_Estado').val();
	var u_Actividad= $('#u_Actividad').val();

	$.ajax({
		url: 'core/data.set.unidad.php',
		type: 'POST',
		data: {Email:u_Email,Nombre:u_Nombre,Telefono:u_Telefono,
		Localidad:u_Localidad,Municipio:u_Municipio,Estado:u_Estado,
		Actividad: u_Actividad},
		success: function(data){
			console.log(data);
		}
	});

}